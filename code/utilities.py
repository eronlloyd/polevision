def rotate_image(image, angle, size=1):
    # Adapted from https://stackoverflow.com/questions/22041699/rotate-an-image-without-cropping-in-opencv-in-c
    height, width = image.shape[:2]
    image_center = (width / 2, height / 2)

    rotated_image = cv2.getRotationMatrix2D(image_center, angle, size)

    abs_cos = abs(rotated_image[0, 0])
    abs_sin = abs(rotated_image[0, 1])

    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)

    rotated_image[0, 2] += bound_w / 2 - image_center[0]
    rotated_image[1, 2] += bound_h / 2 - image_center[1]

    rotated_image = cv2.warpAffine(image, rotated_image, (bound_w, bound_h))

    return rotated_image