

# polevision
Code and notebooks related to my final project for my Master's of
Science in Data Science degree, entitled “Utility Pole Measurements and Feature
Analysis Using Computer Vision and Photogrammetry”

The final report, which which has been successfully presented and defended,
[can be read here](reports/Eron Lloyd--Final Report.pdf).